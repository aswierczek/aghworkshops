﻿using System.Collections.Generic;
using Infusion.MovieScavengerApp.Models;

namespace Infusion.MovieScavengerApp.Services
{
    public interface IMoviesService
    {
        IList<Movie> GetMovies();

        void AddMovie(Movie movie);

        // Extra task: implement deleting
        // void RemoveMovie(Movie movie)
    }
}