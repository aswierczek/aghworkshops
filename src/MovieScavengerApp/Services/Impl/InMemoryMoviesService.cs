﻿using System;
using System.Collections.Generic;
using Infusion.MovieScavengerApp.Models;

namespace Infusion.MovieScavengerApp.Services.Impl
{
    public class InMemoryMoviesService : IMoviesService
    {
        private readonly IList<Movie> _movies = new List<Movie>(); 

        public InMemoryMoviesService()
        {
            _movies.Add(new Movie
            {
                Title = "Batman",
                ReleaseDate = new DateTime(1989, 12, 31),
                Cast = new[]
                {
                    new Actor
                    {
                        Name = "Michael Keaton",
                        BirthDate = new DateTime(1951, 9, 5)
                    },
                    new Actor
                    {
                        Name = "Jack Nickolson",
                        BirthDate = new DateTime(1937, 4, 22)
                    }
                }
            });
            _movies.Add(new Movie
            {
                Title = "Batman Returns",
                ReleaseDate = new DateTime(1992, 6, 16),
                Cast = new[]
                {
                    new Actor
                    {
                        Name = "Michael Keaton",
                        BirthDate = new DateTime(1951, 9, 5)
                    },
                    new Actor
                    {
                        Name = "Michelle Pfeiffer",
                        BirthDate = new DateTime(1958, 4, 29)
                    }
                }
            });
        }

        public IList<Movie> GetMovies()
        {
            return _movies;
        }

        public void AddMovie(Movie movie)
        {
            _movies.Add(movie);
        }
    }
}