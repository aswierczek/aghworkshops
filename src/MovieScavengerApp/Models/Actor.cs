﻿using System;

namespace Infusion.MovieScavengerApp.Models
{
    public class Actor
    {
        public string Name { get; set; }
        
        public DateTime BirthDate { get; set; } 
    }
}